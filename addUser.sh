#!/bin/bash

USERNAME=$1
PASSWORD=$2
grep -q $USERNAME /etc/passwd
if [[ $? -eq 0 ]]
then
 echo "user $USERNAME already exists - Pleae enter a different user name "
 exit 1
fi


useradd $USERNAME
if [[ $? -eq 0 ]]
then
 echo "user $USERNAME was addedd successfully"
else
 echo "Something wrong happened user $USERNAME was not added"
fi



echo $PASSWORD | passwd --stdin $USERNAME &> /dev/null
if [[ $? -eq 0 ]]
then
echo "Password was set successfully for user $USERNAME. and the password is $PASSWORD"
fi
